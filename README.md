# Amdatu Configurator

This project provides a simple way of provisioning configuration files to your OSGi application, allowing you to use the
same set of configuration files to run your OSGi application locally as you would need for deploying your application
using Apache ACE or DeploymentAdmin. 

It provides the following features:

- provisions MetaType/AutoConf based configuration files;
- provisions property-file based configuration files;
- allow simple variable substitutions similar as to Apache ACE supports;
- a simple API to provision configuration files programmatically;
- a couple of simple Felix Gogo commands to list and update configurations.

What this project does *not* provide:

- a FileInstall like functionality, where configuration files and bundles are automatically monitored and updated if
  necessary. The reason for this is that we do not want to mimic the behaviour of FileInstall (FileInstall does this
  already), and that in production systems this functionality is often not desired nor used. However, this functionality
  can be mimicked by using the programmatic API of Amdatu Configurator;
- A set of Gogo shell commands to alter configurations such as provided by [1].


## Building

This project uses [Gradle](http://www.gradle.org) as build system. To build this project from source, you only need to
invoke `gradle build`, or use the included wrapper: `./gradlew build`. The resulting bundles can be found in the
`org.amdatu.configurator/generated` directory.


## Usage

Deploy the `org.amdatu.configurator.autoconf` and/or `org.amdatu.configurator.properties` bundle with your application.
When this bundle is started by the OSGi framework, it will look for a directory named `conf` in the current working
directory of your framework and regard all files ending in `.xml` as possible AutoConf resources. Files ending in
`.cnf`, `.cfg` or `.config` are considered property file resources.

The API of Amdatu Configurator is relatively straight forward, you can use the `org.amdatu.configurator.Configurator`
services to provision a single configuration resource, provision all configuration resources in a given directory, or
list the already provisioned configurations. Distinction between various configurator services can be made by using the
`type` and/or `file.extensions` service properties. For example, you can ask for a configurator that is capable of
handling `.cfg` files by:

    ServiceReference[] serviceRefs =
        bundleContext.getServiceReferences(Configurator.class.getName(), "(file.extensions=.cfg)");

Or, if you want a specific type of configurator (currently `autoconf` and `property` are the only supported types):

    ServiceReference[] serviceRefs =
        bundleContext.getServiceReferences(Configurator.class.getName(), "(type=autoconf)");


### Dependencies

The Amdatu Configurator bundles have the following external dependencies:

1. Apache Felix DependencyManager v3.1.0 or later;
2. An OSGi ConfigurationAdmin service implementation, for example Apache Felix ConfigAdmin v1.8.0;
3. (Optionally) an OSGi LogService implementation, for example Apache Felix LogService v1.0.1.


### Configuration

A couple of configuration options exist for the Amdatu Configurator project. These options can be set as framework
option in the run configuration of your application. For the AutoConf bundle, the following options are recognized:

- `org.amdatu.configurator.autoconf.dir`, takes a string argument that is used as directory where your configuration
  files are stored. Defaults to `./conf`;
- `org.amdatu.configurator.autoconf.prefix`, allows one to specify the common prefix used for all placeholders in your
  configuration files. By default, the same prefix is used as Apache ACE, which is `context.`;
- `org.amdatu.configurator.autoconf.verbose`, if set to `true`, increases the verbosity of logging to show more details
  on the whole provisioning process. By default, non-verbose/sparse logging is enabled (`false`).

For the Properties bundle, the following options are recognized:

- `org.amdatu.configurator.properties.dir`, takes a string argument that is used as directory where your configuration
  files are stored. Defaults to `./conf`;
- `org.amdatu.configurator.properties.prefix`, allows one to specify the common prefix used for all placeholders in your
  configuration files. Defaults to `context.`;
- `org.amdatu.configurator.properties.verbose`, increases the verbosity of logging to show additional details on the
  provisioning process. Defaults to `false`.

In addition, the logging of the bundles be managed as well, by using the following option:

- `org.amdatu.configurator.loglevel`, takes a value of [0..4] denoting the log level to use in which a higher value
  means more verbosity of logging. So, a value of <tt>0</tt> means no logging, <tt>1</tt> to only show errors and so on
  to <tt>4</tt> which shows all messages. Currently, log messages are printed to the console (stdout).


### Variable/placeholder substitution

Though not allowed by the AutoConf specification, the Amdatu Configurator allows AutoConf resources to be parameterized
with variables (or placeholders) in order to make them a little more reusable for different deployment scenarios.
Variables are considered all strings that start with <tt>${</tt> and end with <tt>}</tt>. When an AutoConf resource is
processed by Amdatu Configurator, it will try to replace all variables with values found as framework properties. So,
for example, a variable `${context.serverUrl}` will be replaced with the value of the framework property `serverUrl`
(the `context.` prefix is stripped off, see [configuration](#configuration)). If no framework property is found, no
substitution will take place.


### Example

Consider the following AutoConf resource, named `myConfig.xml`, stored in a directory named `config`:

    <?xml version="1.0" encoding="UTF-8"?>
    <metatype:MetaData xmlns:metatype="http://www.osgi.org/xmlns/metatype/v1.1.0">
        <OCD id="ocd" name="ocd">
            <AD id="server" type="String" />
        </OCD>
    
        <Designate pid="org.amdatu.conf" bundle="org.amdatu.sample.bundle">
            <Object ocdref="ocd">
                <Attribute adref="server" name="serverurl" content="${context.serverUrl}" />
            </Object>
        </Designate>
    </metatype:MetaData>

If we would start our OSGi application, for example, using a Bnd Run Descriptor with the following content:

    -runbundles: \
       osgi.cmpn;version="[4.2,5)",\
       org.apache.felix.configadmin;version="[1.8,1.9)",\
       org.apache.felix.dependencymanager;version="[3.1,3.2)",\
       org.apache.felix.log;version="[1.0.1,1.0.2)",\
       org.amdatu.configurator.autoconf;version=latest
    # ...many more properties...
    -runproperties: \
       serverUrl=http://my.server.host:8080/,\
       org.amdatu.configurator.autoconf.dir=/path/to/config

The Amdatu Configurator will provision a configuration with a single property of `serverUrl =
http://my.server.host:8080/` to ConfigAdmin. The result will be that this property will be given to the ManagedService
implementation registered with the `org.amdatu.conf` PID.


## Links

* [Amdatu Website](http://www.amdatu.org/components/configurator.html);
* [Source Code](https://bitbucket.org/amdatu/amdatu-configurator);
* [Issue Tracking](https://amdatu.atlassian.net/browse/AMDATUCONF);
* [Development Wiki](https://amdatu.atlassian.net/wiki/display/AMDATUDEV/Amdatu+Configurator);
* [Continuous Build](https://amdatu.atlassian.net/builds/browse/AMDATUCONF).


## References

1. [OSGi Configuration Admin command line client](https://bitbucket.org/pjtr/net.luminis.cmc)


## License

The Amdatu Configurator project is licensed under [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0.html).

