/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.configurator.itest;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Dictionary;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.jar.Attributes;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;
import java.util.zip.ZipEntry;

import junit.framework.TestCase;

import org.amdatu.configurator.Configurator;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;
import org.osgi.service.cm.ManagedServiceFactory;

/**
 * Test cases for Amdatu AutoConf.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class AutoConfigTest extends TestCase {
    /**
     * Total number of configurations provisioned by the configuration resources in `conf/`.
     */
    private static final int TOTAL_CONFIGURATION_COUNT = 14;

    private final BundleContext m_context = FrameworkUtil.getBundle(getClass()).getBundleContext();

    /**
     * Tests that we can obtain specific {@link Configurator} services by using their service properties.
     */
    public void testObtainSpecificConfiguratorOk() throws Exception {
        ServiceReference[] serviceRefs;

        serviceRefs = m_context.getServiceReferences(Configurator.class.getName(), "(file.extensions=.cfg)");
        assertNotNull(serviceRefs);
        assertEquals(1, serviceRefs.length);

        serviceRefs = m_context.getServiceReferences(Configurator.class.getName(), "(file.extensions=.xml)");
        assertNotNull(serviceRefs);
        assertEquals(1, serviceRefs.length);

        serviceRefs = m_context.getServiceReferences(Configurator.class.getName(), "(file.extensions=*)");
        assertNotNull(serviceRefs);
        assertEquals(2, serviceRefs.length);

        serviceRefs = m_context.getServiceReferences(Configurator.class.getName(), "(file.extensions=.props)");
        assertNull(serviceRefs);
    }

    /**
     * Tests that the configurations are indeed provisioned.
     */
    public void testConfigurationsProvisionedOk() throws Exception {
        ConfigurationAdmin configAdmin = getConfigAdmin();

        Configuration[] configs = configAdmin.listConfigurations(null);
        assertNotNull("No configurations found?!", configs);
        assertEquals(TOTAL_CONFIGURATION_COUNT, configs.length);

        Configuration config;
        config = configAdmin.getConfiguration("org.amdatu.conf3", null);
        assertNotNull(config);

        assertEquals(m_context.getProperty("context.serverUrl"), config.getProperties().get("serverUrl"));

        // This configuration conatins a "$" but no "{" following the dollar sign
        Configuration dollarConfig;
        dollarConfig = configAdmin.getConfiguration("org.amdatu.conf4", null);
        assertNotNull(dollarConfig);
        assertEquals("$value2", dollarConfig.getProperties().get("key2"));

        // This configuration should fall back to a default attribute value...
        config = configAdmin.getConfiguration("org.amdatu.conf4b", null);
        assertNotNull(config);

        assertEquals("localhost:8080", config.getProperties().get("server"));
    }

    /**
     * Tests that we can list all the provisioned configurations.
     */
    public void testListConfigurationsOk() throws Exception {
        ServiceReference[] serviceRefs = m_context.getServiceReferences(Configurator.class.getName(), null);
        assertNotNull("No Configurator services found?!", serviceRefs);
        assertEquals("Unexpected number of Configurators found?!", 2, serviceRefs.length);

        int total = 0;
        for (ServiceReference serviceRef : serviceRefs) {
            Configurator configurator = (Configurator) m_context.getService(serviceRef);
            try {
                String[] pids = configurator.listProvisioned();
                total += pids.length;
            }
            finally {
                m_context.ungetService(serviceRef);
            }
        }

        assertEquals(TOTAL_CONFIGURATION_COUNT, total);
    }

    /**
     * Tests that a provisioned configuration is actually delivered to the correct managed service.
     */
    public void testObtainAutoConfConfigurationAsManagedServiceFactoryOk() throws Exception {
        final CountDownLatch latch = new CountDownLatch(2);

        ManagedServiceFactory service = new ManagedServiceFactory() {
            @Override
            public void deleted(String pid) {
                fail("Should not occur!");
            }

            @Override
            public String getName() {
                return "My service factory";
            }

            @Override
            public void updated(String pid, Dictionary properties) throws ConfigurationException {
                Object gear = properties.get("gear");
                if (Integer.valueOf(2).equals(gear) || Integer.valueOf(3).equals(gear)) {
                    latch.countDown();
                }
            }
        };

        Properties props = new Properties();
        props.put(Constants.SERVICE_PID, "org.amdatu.conf2.factory");

        ServiceRegistration reg = m_context.registerService(ManagedServiceFactory.class.getName(), service, props);

        assertTrue("No configuration was bound to our service in time?!", latch.await(5, TimeUnit.SECONDS));

        reg.unregister();
    }

    /**
     * Tests that a provisioned configuration is actually delivered to the correct managed service.
     */
    public void testObtainAutoConfConfigurationAsManagedServiceOk() throws Exception {
        final CountDownLatch latch = new CountDownLatch(1);

        ManagedService service = new ManagedService() {
            @Override
            public void updated(Dictionary properties) throws ConfigurationException {
                if (properties != null && "http://conf1:8080/".equals(properties.get("server"))) {
                    latch.countDown();
                }
            }
        };

        Properties props = new Properties();
        props.put(Constants.SERVICE_PID, "org.amdatu.conf1");

        ServiceRegistration reg = m_context.registerService(ManagedService.class.getName(), service, props);

        assertTrue("No configuration was bound to our service in time?!", latch.await(5, TimeUnit.SECONDS));

        reg.unregister();
    }

    /**
     * Tests that a provisioned configuration is actually delivered to the correct managed service.
     */
    public void testObtainPropertiesConfigurationAsManagedServiceFactoryOk() throws Exception {
        final CountDownLatch latch = new CountDownLatch(2);

        ManagedServiceFactory service = new ManagedServiceFactory() {
            @Override
            public void deleted(String pid) {
                fail("Should not occur!");
            }

            @Override
            public String getName() {
                return "My service factory";
            }

            @Override
            public void updated(String pid, Dictionary properties) throws ConfigurationException {
                Object gear = properties.get("gear");
                if ("2".equals(gear) || "3".equals(gear)) {
                    latch.countDown();
                }
            }
        };

        Properties props = new Properties();
        props.put(Constants.SERVICE_PID, "org.amdatu.conf6.factory");

        ServiceRegistration reg = m_context.registerService(ManagedServiceFactory.class.getName(), service, props);

        assertTrue("No configuration was bound to our service in time?!", latch.await(5, TimeUnit.SECONDS));

        reg.unregister();
    }

    /**
     * Tests that a provisioned configuration is actually delivered to the correct managed service.
     */
    public void testObtainPropertiesConfigurationAsManagedServiceOk() throws Exception {
        final CountDownLatch latch = new CountDownLatch(1);

        ManagedService service = new ManagedService() {
            @Override
            public void updated(Dictionary properties) throws ConfigurationException {
                if (properties != null && "http://localhost:9000/".equals(properties.get("server"))) {
                    latch.countDown();
                }
            }
        };

        Properties props = new Properties();
        props.put(Constants.SERVICE_PID, "org.amdatu.conf5");

        ServiceRegistration reg = m_context.registerService(ManagedService.class.getName(), service, props);

        assertTrue("No configuration was bound to our service in time?!", latch.await(5, TimeUnit.SECONDS));

        reg.unregister();
    }

    /**
     * Tests that we cannot provision an AutoConf configuration for bundles that are installed multiple times (having
     * different versions).
     */
    public void testRefuseProvisioningForMultipleBundlesWithSameBSN() throws Exception {
        String bsn = "org.amdatu.configurator.itest.bundle1";

        String loc_bundle1_v1 = createBundle(bsn, "1.0.0");
        Bundle bundle1_v1 = m_context.installBundle(loc_bundle1_v1);
        assertNotNull(bundle1_v1);
        bundle1_v1.start();

        String loc_bundle1_v2 = createBundle(bsn, "2.0.0");
        Bundle bundle1_v2 = m_context.installBundle(loc_bundle1_v2);
        assertNotNull(bundle1_v2);
        bundle1_v2.start();

        File configFile = createAutoConf(bsn, bsn);

        Configurator configurator = getAutoConfConfigurator();
        int count = configurator.listProvisioned().length;

        try {
            assertFalse(configurator.provision(configFile));
            assertEquals(count, configurator.listProvisioned().length);
        }
        finally {
            bundle1_v1.uninstall();
            bundle1_v2.uninstall();
        }
    }

    private File createAutoConf(String bsn, String pid) throws IOException {
        String xml =
            String.format("<?xml version=\"1.0\" encoding=\"UTF-8\"?>" //
                + "<metatype:MetaData xmlns:metatype=\"http://www.osgi.org/xmlns/metatype/v1.1.0\">"
                + "<OCD id=\"ocd\" name=\"ocd\"><AD id=\"server\" type=\"String\" /></OCD>"
                + "<Designate pid=\"%s\" bundle=\"osgi-dp:%s\">"
                + "<Object ocdref=\"ocd\"><Attribute adref=\"server\" name=\"serverurl\" content=\"localhost:8080\" /></Object>"
                + "</Designate></metatype:MetaData>", bsn, pid);

        File file = File.createTempFile("config", ".xml");
        file.deleteOnExit();

        Files.write(file.toPath(), xml.getBytes());

        return file;
    }

    private String createBundle(String bsn, String version) throws IOException {
        File file = File.createTempFile(bsn, ".jar");
        file.deleteOnExit();

        Manifest manifest = new Manifest();
        Attributes attrs = manifest.getMainAttributes();
        attrs.put(Attributes.Name.MANIFEST_VERSION, "1");
        attrs.putValue("Bundle-ManifestVersion", "2");
        attrs.putValue("Bundle-SymbolicName", bsn);
        attrs.putValue("Bundle-Version", version);

        try (FileOutputStream fos = new FileOutputStream(file); JarOutputStream jos = new JarOutputStream(fos, manifest)) {
            jos.putNextEntry(new ZipEntry("empty/"));
            jos.closeEntry();
        }

        return file.toURI().toASCIIString();
    }

    private ConfigurationAdmin getConfigAdmin() {
        ServiceReference serviceRef = m_context.getServiceReference(ConfigurationAdmin.class.getName());
        assertNotNull("No ConfigAdmin found?!", serviceRef);

        return (ConfigurationAdmin) m_context.getService(serviceRef);
    }

    private Configurator getAutoConfConfigurator() throws InvalidSyntaxException {
        ServiceReference[] serviceRefs = m_context.getServiceReferences(Configurator.class.getName(), "(type=autoconf)");
        assertNotNull("No AutoConf configurator found?!", serviceRefs);
        assertEquals("No AutoConf configurator found?!", 1, serviceRefs.length);

        return (Configurator) m_context.getService(serviceRefs[0]);
    }
}
