/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.configurator.util;

/**
 * Represents a resource that is to be provisioned.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public interface Resource {

    /**
     * @return the service PID of this resource, never <code>null</code>.
     */
    String getPid();

    /**
     * @return the (optional) factory service PID that created this resource, can be <code>null</code> in case a
     *         'normal' service PID created this resource.
     */
    String getFactoryPid();
}
