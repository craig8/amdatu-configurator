/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.configurator;

import aQute.bnd.annotation.ProviderType;

/**
 * Constants used in Amdatu-Configurator.
 */
@ProviderType
public interface ConfiguratorConstants {
    /**
     * Represents a String service property that distinguishes the various registered {@link Configurator}s.
     * Implementations <b>must</b> provide this service property.
     */
    String CONFIGURATOR_TYPE = "type";
    /**
     * Represents a String-array service property that descripes what file extensions are supported by a
     * {@link Configurator} implementation. Implementations are encouraged to provide this service property.
     */
    String CONFIGURATOR_FILE_EXTENSIONS = "file.extensions";
}
