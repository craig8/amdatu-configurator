/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.configurator.autoconf;

import static org.amdatu.configurator.autoconf.MetaTypeUtil.getContent;
import static org.amdatu.configurator.autoconf.MetaTypeUtil.getValue;
import static org.amdatu.configurator.util.LoggerHelper.warn;

import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.amdatu.configurator.util.Resource;
import org.apache.felix.metatype.AD;
import org.apache.felix.metatype.Attribute;
import org.apache.felix.metatype.Designate;
import org.apache.felix.metatype.DesignateObject;
import org.apache.felix.metatype.OCD;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

/**
 * Represents a single configuration in AutoConf format.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class AutoConfResource implements Resource {
    private static final String LOCATION_PREFIX = "osgi-dp:";

    private final Designate m_designate;
    private final OCD m_ocd;
    private String m_actualBundleLocation;

    /**
     * Creates a new {@link AutoConfResource} instance.
     */
    public AutoConfResource(Designate designate, OCD ocd) {
        if (designate == null) {
            throw new IllegalArgumentException("Invalid designate!");
        }

        DesignateObject object = designate.getObject();
        if (ocd != null && !ocd.getID().equals(object.getOcdRef())) {
            throw new IllegalArgumentException("Invalid OCD!");
        }

        m_designate = designate;
        m_ocd = ocd;
    }

    private static boolean isEmpty(String str) {
        return str == null || "".equals(str.trim());
    }

    /**
     * Returns the bundle location the AutoConf resource should be provisioned to.
     * <p>
     * Locations starting with the prefix "osgi-dp:" are stripped from this prefix, allowing AutoConf resources used in
     * Deployment Admin to be used as well.
     * </p>
     * 
     * @return the bundle location, can be <code>null</code> in case the bundle location couldn't be determined.
     */
    public String getBundleLocation() {
        if (m_actualBundleLocation == null) {
            String loc = m_designate.getBundleLocation();
            if ((loc != null) && loc.startsWith(LOCATION_PREFIX)) {
                loc = loc.substring(LOCATION_PREFIX.length());

                Bundle bundle = findBundleByBSN(loc);
                if (bundle != null) {
                    m_actualBundleLocation = bundle.getLocation();
                }
                else {
                    warn("Bundle \"%s\" not installed! Not able to determine its location!", loc);
                }
            }
            else {
                m_actualBundleLocation = loc;
            }
        }
        return m_actualBundleLocation;
    }

    /**
     * @return the actual configuration properties, never <code>null</code>.
     */
    public Dictionary<String, ?> getConfiguration() {
        DesignateObject object = m_designate.getObject();

        Map<String, Attribute> attributes = createAttributeMapping(object);
        Map<String, AD> attributeDefs = m_ocd.getAttributeDefinitions();

        Dictionary<String, Object> props = new Hashtable<>(attributeDefs.size());
        for (Map.Entry<String, AD> entry : attributeDefs.entrySet()) {
            String adRef = entry.getKey();
            AD attributeDef = entry.getValue();
            Attribute attribute = attributes.get(adRef);

            String[] content = getContent(attribute, attributeDef);

            if (content != null) {
                props.put(adRef, getValue(content, attributeDef.getType(), attributeDef.getCardinality()));
            }
        }

        return props;
    }

    /**
     * @return the designate for this resource, never <code>null</code>.
     */
    public Designate getDesignate() {
        return m_designate;
    }

    @Override
    public String getFactoryPid() {
        return m_designate.getFactoryPid();
    }

    /**
     * @return a "unique" identifier for this AutoConf resource, never <code>null</code>.
     */
    public String getId() {
        String id = m_designate.getFactoryPid();
        if (isEmpty(id)) {
            id = m_designate.getPid();
        }
        else {
            id = id + "/" + m_designate.getPid();
        }
        return id;
    }

    @Override
    public String getPid() {
        return m_designate.getPid();
    }

    /**
     * @return <code>true</code> if this AutoConf resource represents a factory configuration, <code>false</code> if it
     *         represents a "singleton" configuration.
     */
    public boolean isFactoryConfig() {
        String factoryPid = m_designate.getFactoryPid();
        return !isEmpty(factoryPid);
    }

    /**
     * Verifies whether this resource is "correct" according to its OCD.
     * 
     * @return <code>true</code> if the verification was successful, <code>false</code> otherwise.
     */
    public boolean verify(boolean verifyBundleLocation) {
        String pid = getId();
        // We *must* have something to validate...
        DesignateObject object = m_designate.getObject();
        if (object == null) {
            warn("Designate Object child missing or invalid for Designate \"%s\"", pid);
            return false;
        }

        // The attribute ocdRef itself is mandatory and cannot be omitted, but its value
        // can still be empty...
        String ocdRef = object.getOcdRef();
        if ("".equals(ocdRef)) {
            warn("Object ocdRef attribute missing or invalid for Designate \"%s\"", pid);
            return false;
        }

        if (m_ocd == null) {
            warn("No OCD defined or found for Designate \"%s\"", pid);
            // Since we cannot validate anything, presume it is valid...
            return true;
        }

        String bundleLocation = m_designate.getBundleLocation();
        if (isEmpty(bundleLocation) || LOCATION_PREFIX.equals(bundleLocation)) {
            warn("Bundle location not set for Designate \"%s\": AutoConf resource processor will not accept this!", pid);
            return false; // this is not what we want to support
        }
        else if ("*".equals(bundleLocation)) {
            warn("Designate \"%s\" is to be bound to the installing bundle, which might not be as expected?!", pid);
            return false; // this is not what we want to support
        }
        else if (!bundleLocation.startsWith(LOCATION_PREFIX)) {
            warn("Designate \"%s\" is not useable by an AutoConf resource processor: bundle location does not start with 'osgi-dp:'!", pid);
            return false; // this is not what we want to support
        }
        else {
            // See whether the mentioned bundle is available; keep going to find additional stuff only if the bundle is
            // there...
            if (verifyBundleLocation && !bundlePresent(bundleLocation.substring(LOCATION_PREFIX.length()))) {
                return false;
            }
        }

        Map<String, AD> attributeDefs = m_ocd.getAttributeDefinitions();
        if (attributeDefs == null) {
            warn("OCD \"%s\" does not define a single AD", m_ocd.getID());
            return false;
        }

        Map<String, Attribute> attributes = new HashMap<>();

        List<Attribute> attributeList = object.getAttributes();
        if (attributeList != null) {
            for (Attribute attribute : attributeList) {
                if (attributes.put(attribute.getAdRef(), attribute) != null) {
                    warn("Duplicate Object Attribute \"%s\" in Designate \"%s\"", attribute.getAdRef(), pid);
                    return false;
                }
            }
        }

        // Some additional checks specific for AutoConf processing
        if (m_designate.isMerge()) {
            warn("Merging of Objects not supported (Designate: \"%s\")!", pid);
            return false;
        }

        // Validate attributes...
        for (Map.Entry<String, AD> entry : attributeDefs.entrySet()) {
            if (!verifyAttribute(entry.getValue(), attributes.get(entry.getKey()))) {
                return false;
            }
        }

        return true;
    }

    private boolean bundlePresent(String bsn) {
        Bundle bundle = findBundleByBSN(bsn);
        return (bundle != null);
    }

    private Map<String, Attribute> createAttributeMapping(DesignateObject object) {
        Map<String, Attribute> attributes = new HashMap<>();
        List<Attribute> attributeList = object.getAttributes();
        if (attributeList != null) {
            for (Attribute attribute : attributeList) {
                attributes.put(attribute.getAdRef(), attribute);
            }
        }
        return attributes;
    }

    private Bundle findBundleByBSN(String bsn) {
        Bundle bundle = FrameworkUtil.getBundle(getClass());
        if (bundle == null) {
            warn("Unable to determine whether bundle \"%s\" is installed; not running in an OSGi context?!", bsn);
            return null;
        }

        Bundle[] bundles = bundle.getBundleContext().getBundles();
        Bundle result = null;
        for (Bundle b : bundles) {
            if (bsn.equals(b.getSymbolicName())) {
                if (result != null) {
                    warn("Bundle \"%s\" is installed multiple times with different versions! Cannot provision AutoConf resource with Designate \"%s\"!", bsn, getId());
                    return null;
                }
                result = b;
            }
        }
        return result;
    }

    private boolean verifyAttribute(AD attributeDef, Attribute attribute) {
        if (attribute == null && attributeDef.isRequired()) {
            // wrong attribute or definition
            warn("No attribute found for AD \"%s\" in Designate \"%s\"", attributeDef.getID(), getId());
            return false;
        }

        try {
            String[] content = getContent(attribute, attributeDef);
            Object value = getValue(content, attributeDef.getType(), attributeDef.getCardinality());

            if (value == null && attributeDef.isRequired()) {
                warn("No value for attribute \"%s\" in Designate \"%s\"", attributeDef.getID(), getId());
                return false;
            }
        }
        catch (IllegalArgumentException e) {
            warn("%s for attribute \"%s\" in Designate \"%s\"", e.getMessage(), attributeDef.getID(), getId());
            return false;
        }

        return true;
    }
}
